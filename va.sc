fn va-map (f arg ...)
    """"Takes a function `f`, that can take one argument, and returns the value of each following argument applied to `f`
        ### Example:
        ```
        # write:
        let x y z =
            va-map local u8 u16 u32

        # instead of:
        let x y z =
            local u8
            local u16
            local u32
        ```
    if (not (none? arg))
        return
            f arg
            if (not (va-empty? ...))
                recur f ...

fn va-map-functions (f0 f...)
    if (va-empty? f...)
        f0
    else
        fn (args...)
            return
                f0 args...
                (va-map-functions f...) args...

fn va-reverse (...)
    if (not (va-empty? ...))
        let first args... = ...
        if (va-empty? args...)
            first
        else
            (va-join (va-reverse args...)) first

fn va-slice (start end ...)
    let loop (i result...) = start
    if (i < end)
        let val = (va@ i ...)
        loop (i + 1) ((va-join result...) val)
    result...

fn va-cycle (shift ...)
    let first... = (va@ shift ...)
    let rest... = (va-slice 0 shift ...)
    (va-join first...) rest...

fn va-pos (val args...)
    let loop (i) = 0
    let in-val = (va@ i args...)
    if (none? in-val)
        none
    else
        if (val == in-val)
            i
        else
            loop (i + 1)

fn va-range (start end)
    let loop (i rest...) = start
    if (i < end)
        loop (i + 1) i rest...
    va-reverse rest...

fn va-swap (i j ...)
    let first... =
        va-slice 0 i ...
    let a second... =
        va-slice i j ...
    let b third... =
        va@ j ...
    (va-join ((va-join first...) b second...)) a third...

fn va-find (pred data...)
    let len = (va-countof data...)
    let loop (i) = 0
    if (i < len)
        let val = (va@ i data...)
        if (pred val)
            return i val
        loop (i + 1)
    len

fn va-remove (pred data...)
    let len = (va-countof data...)
    let loop (i result...) = 0
    if (i < len)
        let val = (va@ i data...)
        if (pred val)
            loop (i + 1) result...
        else
            loop (i + 1) val result...
    result...

fn va-remove-doubles (a b ...)
    if (not (and (none? a) (none? b)))
        return
            (va-join (if (a != b) a)) (recur b ...)

fn va-all (pred first ...)
    if (va-empty? ...)
        pred first
    else
        and
            pred first
            va-all pred ...

fn va-count (pred ...)
    let len = (va-countof ...)
    let loop (i count) = 0 0
    if (i < len)
        let new-count =
            (pred (va@ i ...)) as (typeof count)
        loop (i + 1) new-count
    count

fn va-sort (f first data...)
    if (va-empty? data...)
        return first
    let pred =
        fn (val) (f first val)
    let sorted-data... =
        recur f data...
    let pos =
        va-find pred sorted-data...
    (va-join (va-slice 0 pos sorted-data...)) first (va@ pos sorted-data...)

#fn va-sort (f data...)
    fn quicksort (data...)
        fn devide (data...)
            let left right = 0 ((va-countof data...) - 1)
            let pivot = (va@ right data...)
            let loop (i j data...) = left (right - 1) data...
            let inner (i) = i
            if ((f (va@ i data...) pivot) and (i < (right - 1)))
                inner (i + 1)
            let inner (j) = j
            if ((f pivot (va@ j data...)) and (left < j))
                inner (j + 1)
            
            if (< i j)
                loop
                    va-swap i j data...
            return i (va-swap i right data...)
        let i data... =
            devide data...
        dump i data...
        let left... =
            va-slice 0 i data...
        let left... =
            quicksort left...
        let right... =
            quicksort (va@ i data...)
        (va-join left...) right...
    quicksort data...

fn va-times (num value)
    let loop (i result...) = 0
    if (i < num)
        loop (i + 1) value result...
    result...

if main-module?
    print
        va-slice 1 4
            \ 0 1 2 3 4

    print
        va-swap 2 4
            \ 0 1 2 3 4 5

    print
        va-find (fn (x) (x < 5))
            \ 10 9 8 7 6 5 4 3 2 1 0
    
    print
        va-sort (do <)
            \ 5 4 3 6 7 2 1
    
    print
        va-remove-doubles 1 1 1 1 2 2 3 3 4 5 6 6 6 6 6 7 8 8
    
    print
        va-remove-doubles
            va-sort (do >)
                \ 1 2 1 2 2 1 3 2 3 2 2 1 2 3 1 2 3 4 5 2 3 1
    
    fn even? (arg)
        and
            (typeof arg) < integer
            (arg % 2) == 0
    print
        va-remove even? 2 3 4 5 6 7 8

locals;


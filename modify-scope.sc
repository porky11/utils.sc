
fn modify-scope (scope f)
    let new-scope = (Scope)
    for k v in scope
        let string = (k as Symbol as string)
        let new-string valid = (f string)
        if (not valid)
            continue;
        set-scope-symbol! new-scope (string->Symbol new-string) v
    new-scope

if main-module?
    import .remove-prefix
    using import .string-convert
    let libs =
        import-c "lib.c"
            """"
                #include <GL/gl.h>
                #include <vulkan/vulkan.h>
            '()

    let gl vk =
        modify-scope libs 
            fn (string) 
                let string valid = (remove-prefix "gl" string)
                let string =
                    unconst
                        if valid
                            lowercase (to-lisp string)
                        else ""
                return string valid
        modify-scope libs (fn (string) (remove-prefix "vk" string))

    print "Some symbols in gl scope:"
    let i = (local u8)
    for k v in gl
        if (i == 0)
            print k
        i = i + 2

    print "Some symbols in vk scope:"
    for k v in vk
        if (i == 0)
            print k
        i = i + 2

modify-scope

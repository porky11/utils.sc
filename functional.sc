#!/usr/bin/env scopes

using import .va

fn curry (f now...)
    fn (later...)
        f ((va-join now...) later...)

fn compose (f funs...)
    if (va-empty? funs...) f
    else
        fn (args...)
            f ((compose funs...) args...)

fn compose-apply (op f...)
    fn (args...)
        op
            (va-map-functions f...) args...

fn complement (...)
    compose not ...

fn value (val)
    fn (...)
        val

fn all (pred first ...)
    if (va-empty? ...)
        pred first
    else
        and
            pred first
            all pred ...

fn any (pred first ...)
    if (va-empty? ...)
        pred first
    else
        or
            pred first
            any pred ...
            
                


if main-module?
    fn test (a b)
        a - b
    
    let subtract-of-1 =
        curry test 1
    let subtract-of-3 =
        curry test 3
    
    assert
        and
            (subtract-of-1 1) == 0
            (subtract-of-3 2) == 1
    
    let subtract-1 =
        curry test
            b = 1
    assert
        and
            (subtract-1 2) == 1
            (subtract-1 (a = 0)) == -1
    
    let nn =
        compose not none?
    
    assert
        nn '()
    

    #fn evens (...)
        let loop (i args...) = 0 ...
        print args...
        if (va-empty? (va@ i args...))
            return args...
        else
            if (even? (va@ i args...))
                loop (i + 1) args...
            else
                loop i (va-join (va@ (i + 1) args...)
    
    
    fn print-and-return (to-print return...)
        print to-print
        return...
    
    let print-and-sum =
        compose (do +) print-and-return
    
    assert
        == 10
            print-and-sum "Hello" 1 2 3 4
    
    fn x+y (x y)
        x + y

    fn ab-to-xy (a b)
        return
            x = a
            y = b
    
    let a+b =
        compose x+y ab-to-xy
    
    assert
        ==
            a+b
                a = 1
                b = 2
            3
    
    let a b c d e = (va-reverse 1 2 3 4 5)
    assert
        and
            a == 5
            b == 4
            c == 3
            d == 2
            e == 1
    
    fn call-and-args (f ...)
        return
            f ...
            ...
    
    let reverse-sum-and-print =
        compose print (curry call-and-args +) va-reverse

    reverse-sum-and-print 1 2 3 4 5

    fn even? (arg)
        and
            (typeof arg) < integer
            (arg % 2) == 0

    let odd? =
        complement even?
    

    assert 
        == 
            (va-map-functions (do +) (do *)) 1 2 3


    assert (all even? 2 4 6)
    assert (not (all even? 1 2 3 4))
    assert (any odd? 1 2 3 4)
    assert (not (any odd? 2 4 6))
    
    let avg = 
        compose-apply (do /) (do +) va-countof
    
    assert
        ==
            3.0
            avg 1 2 3 4 5

fn args (args...)
    fn (f)
        f args...

locals;




using import .casing

fn to-lisp (string)
    let len = (countof string)
    let new-len = (local usize len)
    do
        let small = (local bool false)
        for i in (range 0:usize len)
            let char = (string @ i)
            let large = (uppercase? char)
            if ((small as bool) and large)
                new-len = new-len + 1
            small = (lowercase? char)
    let new-str =
        alloca-array i8 (new-len + 1)

    do
        let small = (local bool false)
        let pos = (local u32)
        for i in (range 0:usize len)
            let c = (string @ i)
            let large = (uppercase? c)
            let res = ((small as bool) and large)
            if res
                new-str @ pos = (char "-")
                pos = pos + 1
            new-str @ pos = 
                do
                    if (c == (char "_")) (char "-")
                    else
                        if res (lowercase c)
                        else c
            pos = pos + 1
            small = (lowercase? c)
    
    new-str @ new-len = 0
    
    string.from-cstr (new-str as rawstring)

if main-module?
    assert ((to-lisp "ABC_DEF") == "abc-def")
    assert ((to-lisp "camelCaseFunction1") == "camel-case-function1")
    assert ((to-lisp "CamelCaseFunction2") == "camel-case-function2")
    assert ((to-lisp (unconst "FnABC_DEF")) == "fn-abc-def")

locals;


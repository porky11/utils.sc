using import .va

fn make-multi (f)
    """"Takes a function `f`, that takes one argument, and returns a function, that takes multiple arguments, that calls `f` on multiple arguments, and returns all values.
        
        ### Example:
        ```
        let not = (make-multi not) # define multiple value not
        
        let a b =
            not false true
        assert
            and
                (a == true)
                (b == false)
        ```
    fn (...)
        va-map  f ...

let make-multis =
    make-multi make-multi

fn make-multi-function (f)
    fn mf (arg ...)
        return
            f arg
            if (not (va-empty? ...))
                mf ...

let make-multi-functions =
    make-multi-function make-multi-function

if main-module?

    let a b c d =
        va-map  not false false true false
    
    assert
        and
            a == true
            b == true
            c == false
            d == true
    
    fn print-ret (val)
        print val
        val
    let a b c =
        print-ret 1
        print-ret 2
        print-ret 3

    assert
        and
            a == 1
            b == 2
            c == 3
    
    let print-rets =
        make-multi print-ret
    
    let x y z =
        print-rets 1 2 3

    assert
        and
            a == 1
            b == 2
            c == 3


locals;

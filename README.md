Some utility functions for [scopes](scopes.rocks).
Some may be outsourced into single libraries later or may even be useful enough to be included in core.
Most files contain examples for usage.

_casing.sc_:

Functions for converting alphabetic characters and strings to uppercase or lowercase, and do case checking for characters.

_remove-prefix.sc_:

A function for removing a prefix from a string. The prefix is specified case insensitive.

_string-convert.sc_:

Convert strings to different formats.
Currently just contains `to-lisp`, which converts arbitrary notations (`camelCase` or `WITH_UNDERSCORES`) to a `lispy-notation`. (may lead to weird names in some cases. usage is not recommended)

_modify-scope.sc_:

A function to modify all symbols of a scope. (takes a scope and a function, that takes a string and returns two values: The new symbol name as a string and a boolean to decide, if the symbol will be set.

_argless.sc_:

Defines a function to convert functions, that need at least two arguments, into functions, that just need single arguments or even none.

_math.sc_:

Some simple mathematical functions.

_multi.sc_:

Defines functions to redefine single argument functions with a single return value as multi argument functions with multiple return values.

_va.sc_:

Utility functions for variadic functions.
Some will hopefully be included in core.



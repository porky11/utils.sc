fn even? (arg)
    and
        (typeof arg) < integer
        (arg % 2) == 0

fn odd? (arg)
    and
        (typeof arg) < integer
        (arg % 2) == 1

fn factorial (n)
    let loop (i result) = 1 1
    if (i <= n)
        loop (i + 1) (result * i)
    result


fn over (n k)
    let op =
        if ((typeof n) < integer and (typeof k) < integer)
            do //
        else
            do /
    op
        op
            factorial n
            factorial (- n k)
        factorial k
define-infix> 550 over 

if main-module?
    using import utils.va
    print
        va-map factorial 0 1 2 3 4

    print
        0 over 0 # 1
        16 over 16 # 1
        4 over 3 # 4

locals;



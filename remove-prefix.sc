#!/usr/bin/env scopes

using import .casing

fn remove-prefix (prefix string)
    """"Removes `prefix` from `string`.
        Both arguments are strings.

        A second return value shows, if the string contains a valid prefix.
        A prefix is also invalid, if the prefix is not explicitely seperated from the string.
        
        This is useful for renaming imported symbols from c.
        
        The specified casing of `prefix` is not important.
        In case `prefix = "prefix"`, the following names will be converted this way:
        * prefix_name => name
        * prefixname invalid
        * prefixName => name
        * PrefixName => Name
        * PREFIX_NAME => NAME
        * PREFIXNAME invalid

    let prefix-len =
        countof prefix
    let valid =
        unconst
            and
                (uppercase (slice string 0 prefix-len)) == (uppercase prefix)
                or
                    not
                        lowercase? (string @ prefix-len)
                    uppercase? (slice string 0 prefix-len)
                not
                    and
                        uppercase? (slice string prefix-len)
                        uppercase? (string @ prefix-len)

    let string =
        unconst
            if valid
                let start =
                    if ((string @ prefix-len) == (char "_"))
                        prefix-len + 1
                    else
                        prefix-len

                if (lowercase? (string @ 0))
                    let c =
                        allocaof
                            lowercase (string @ start)
                    ..
                        string-new c 1:usize
                        slice string (start + 1)
                else
                    slice string start
            else ""
    return string valid

if main-module?
    assert ((remove-prefix "my" "MyStructName") == "StructName")
    assert ((remove-prefix "my" "myFunctionName") == "functionName")
    assert ((remove-prefix "my" "MY_CONST_NAME") == "CONST_NAME")
    assert ((remove-prefix "my" "MYtype") == "type")
    assert (not (va@ 1 (remove-prefix "my" "myinvalid")))
    assert (not (va@ 1 (remove-prefix "my" "MYINVALID")))

remove-prefix


define-macro ign
    """"Ignores all of it's arguments.
        Some other way for commenting, in case you don't need the function.
        It's still important to have matching brackets and correct syntax.
    list do

let old-print = print

fn print (...)
    """"Prints all supplied arguments seperated by space and afterwards prints a newline.
        Returns all arguments, so printing is can still be used afterwards.
    old-print ...
    ...

#let unconst = 
    make-multi-function unconst

#fn sort (data)
    """"An unstable sort function.
    let loop (left right) = 0 ((countof data) - 1)
    

fn some (val ...)
    """"Returns the first not-none value.
    if (none? val)
        recur ...
    else
        val

fn storage (value)
    bitcast value
        storageof (typeof value)

typefn integer '__call (self arg)
    let loop (i result...) = 0
    if (i < self)
        loop (i + 1) arg result...
    result...

if main-module?
    ign (print (unimplemented-function (a + b)))
    
    define use-graphics false
    define use-terminal true
    define use-sound false
    
    let interface =
        some
            if use-graphics 'graphical-interface
            if use-terminal 'terminal
            if use-sound 'sound-engine
    
    if (none? interface)
        error! "No valid interface found"
    print "Setting up interface successful!"
    #todo next:
        text-program.run-with-text-function (get-text-function interface)
    
    print
        3 "hello"

fn unify-scopes (scopes...)
    let new-scope = (Scope)
    let loop (scope scopes...) = scopes...
    if (not (none? scope))
        for k v in scope
            set-scope-symbol! new-scope k v
        loop scopes...
    new-scope 

locals;

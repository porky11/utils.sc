fn make-transitive (op f)
    """"Creates a variadic function from a transitive binary function.
    fn (a b ...)
        if (va-empty? ...)
            f a b
        else
            op
                f a b
                (make-transitive op f) b ...

let == != <= < > >= =
    make-transitive (do &) ==
    make-transitive (do |) !=
    make-transitive (do &) <=
    make-transitive (do &) <
    make-transitive (do &) >
    make-transitive (do &) >=

if main-module?
    print "Testing comparison operators with multiple values"
    fn test-print (name f ...)
        print (if (none? name) "arguments: ") (else ("test `" .. name .. "`:`"))
            f 1 2 3 4
            f 1 1 2
            f 1 2 1
            f 4 4 4 4
        if (not (va-empty? ...))
            test-print ...

    test-print none list "==" (do ==) "!=" (do !=) "<=" (do <=) "<" (do <) ">" (do >) ">=" (do >=)

locals;


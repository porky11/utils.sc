#!/usr/bin/env scopes

fn lowercase?

fn uppercase? (c)
    if (type== (typeof c) i8)
        and
            <= (char "A") c 
            <= c (char "Z")
    elseif (type== (typeof c) string)
        let len = (countof c)
        let loop (i) = (unconst 0:usize)
        let char = (c @ i)
        if (lowercase? char)
            return (unconst false)
        if (i < len)
            loop (i + 1)
        (unconst true)
    else
        error! "Type has to be a char or a string"

fn lowercase? (c)
    if (type== (typeof c) i8)
        and
            <= (char "a") c
            <= c (char "z")
    elseif (type== (typeof c) string)
        let len = (countof c)
        let ret =
            for i in (range (0 as usize) (len as usize))
                let char = (c @ i)
                if (uppercase? char)
                    return false
        none? ret
    else
        error! "Type has to be a char or a string"
    

define case-diff
    (char "a") - (char "A")

fn uppercase (c)
    if (type== (typeof c) i8)
        if (lowercase? c)
            c - case-diff
        else c
    elseif (type== (typeof c) string)
        let len = (countof c)
        let str =
            alloca-array i8 (len + 1)
        str @ len = 0
        for i in (range (0 as usize) (len as usize))
            (str @ i) = (uppercase (c @ i))
        string.from-cstr (str as rawstring)
    else
        error! "Type has to be a char or a string"
    
fn lowercase (c)
    if (type== (typeof c) i8)
        if (uppercase? c)
            c + case-diff
        else c
    elseif (type== (typeof c) string)
        let len = (countof c)
        let str =
            alloca-array i8 (len + 1)
        str @ len = 0
        for i in (range (0 as usize) (len as usize))
            (str @ i) = (lowercase (c @ i))
        string.from-cstr (str as rawstring)
    else
        error! "Type has to be a char or a string"

if main-module?
    assert ((uppercase "ABCxyz123") == "ABCXYZ123")
    assert ((lowercase "ABCxyz123") == "abcxyz123")
    assert (uppercase? "ABC")
    assert (lowercase? "xyz")
    assert (uppercase? "123")
    assert (lowercase? "123")
    assert (not (uppercase? "ABCxyz123"))
    assert (not (lowercase? "ABCxyz123"))

locals;
